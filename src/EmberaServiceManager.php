<?php

namespace Drupal\embera;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Site\Settings;
use Embera\Embera;
use Embera\Cache\Filesystem;
use Embera\Http\HttpClient;
use Embera\Http\HttpClientCache;

/**
 * Class ServiceManager.
 *
 * @package Drupal\platform_files
 */
class EmberaServiceManager {

  /**
   * The Embera object.
   *
   * @var \Embera\Embera
   */
  protected $embera;

  /**
   * EmberaServiceManager constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file_system service.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings service.
   */
  public function __construct(FileSystemInterface $fileSystem, Settings $settings) {
    $writablePath = $fileSystem->getTempDirectory();
    if (!$settings->get('embera.file.cache.disabled', FALSE)) {
      $duration = $settings->get('embera.file_cache.duration', 3600);
      $httpCache = new HttpClientCache(new HttpClient());
      $httpCache->setCachingEngine(new Filesystem($writablePath, $duration));
      $configuration = $settings->get('embera.class.configuration', []);
      $this->embera = new Embera($configuration, NULL, $httpCache);
    }
    else {
      $configuration = $settings->get('embera.class.configuration', []);
      $this->embera = new Embera($configuration);
    }
  }

  /**
   * Get information from the OEmbed.
   *
   * @param string $url
   *   The url to use for retrieving information.
   *
   * @return array
   *   Returns an array with the retrieved information.
   */
  public function getEmbedInformation($url) {
    static $urls;
    if (!isset($urls[$url])) {
      $urls[$url] = NULL;
      $results = $this->embera->getUrlData([$url]);
      if ($results) {
        $urls[$url] = reset($results);
      }
    }
    return $urls[$url];
  }

  /**
   * Get the thumbnail of a media url.
   *
   * @param string $url
   *   The url of the media.
   *
   * @return string|false
   *   Returns the url of the thumbnail. FALSE if it could not be retrieved.
   */
  public function getThumbnailUrl($url) {
    $details = $this->getEmbedInformation($url);
    if (empty($details['thumbnail_url'])) {
      return FALSE;
    }
    return $details['thumbnail_url'];
  }

  /**
   * Get the title of a video url.
   *
   * @param string $url
   *   The url of the media.
   *
   * @return string
   *   Returns the title of the media.
   */
  public function getTitle($url) {
    $details = $this->getEmbedInformation($url);

    if (!empty($details['title'])) {
      return $details['title'];
    }

    if (!empty($details['description'])) {
      return $details['description'];
    }

    return '';
  }

  /**
   * Get the embed code of a video.
   *
   * @param string $url
   *   The url of the media.
   *
   * @return string
   *   Returns the HTML embed code.
   */
  public function getEmbedCode($url) {
    return $this->embera->autoEmbed($url);
  }

}
